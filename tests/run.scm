(use test extras)
(include "../directory-tree-impl")

(let ([root-dir (create-temporary-directory)])
  (test-assert "simple create-directory-tree works"
    (begin
      (create-directory-tree root-dir '(foo (bar)))
      (and (directory? (make-pathname root-dir "foo"))
           (regular-file? (make-pathname (list root-dir "foo") "bar")))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [perm #o755])
  (test-assert "create-directory-tree file permissions work"
        (begin
          (create-directory-tree root-dir `(foo #:mode ,perm))
          (= perm
             (bitwise-and (file-permissions (make-pathname root-dir "foo"))
                          perm))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [str "hi there\n"])
  (test "create-directory-tree with string file content works"
        str
        (begin
          (create-directory-tree root-dir `(foo ,str))
          (with-input-from-file (make-pathname root-dir "foo") read-string)))
  (delete-directory root-dir #:recurse))

(let* ([root-dir (create-temporary-directory)]
       [str "hi there\n"]
       [proc (lambda (port) (display str port))])
  (test "create-directory-tree with proc for file content works"
        str
        (begin
          (create-directory-tree root-dir `(foo ,proc))
          (with-input-from-file (make-pathname root-dir "foo") read-string)))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [perm #o777])
  (test "create-directory-tree directory permissions work"
        perm
        (begin
          (create-directory-tree root-dir `(foo #:mode ,perm ()))
          (bitwise-and perm
                       (file-permissions (make-pathname root-dir "foo")))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)])
  (test-error "bogus list fails for create-directory-tree"
              (create-directory-tree root-dir '(foo bar baz)))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)])
  (test-assert "simple check-directory-tree works"
    (begin
      (create-directory-tree root-dir '(foo (bar)))
      (check-directory-tree root-dir '(foo (bar)))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)])
  (test-assert "check-directory-tree with #:mode works"
    (begin
      (create-directory-tree root-dir '(foo #:mode #o777))
      (check-directory-tree root-dir '(foo #:mode #o777))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [spec '(foo (bar (baz #:symlink "bar")))])
  (test-assert "check-direcotry-tree with #:symlink works"
    (begin
      (create-directory-tree root-dir spec)
      (check-directory-tree root-dir spec)
      ))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [spec '(foo ())])
  (test-assert "check-directory-tree checks for directories"
    (begin
      (create-directory-tree root-dir spec)
      (check-directory-tree root-dir spec)))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [spec '(foo (bar (baz (qux))))])
  (test-assert "check-directory-tree checks for deep directories"
    (begin
      (create-directory-tree root-dir spec)
      (check-directory-tree root-dir spec)))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)])
  (test-error "bogus list fails for check-directory-tree"
              (begin (create-directory-tree root-dir '(foo))
                     (check-directory-tree root-dir '(foo bar baz))))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [spec '(foo ((bar ((baz (file))))))])
  (test-assert "create-directory-tree for files and folders works"
    (begin
      (create-directory-tree root-dir spec)
      (check-directory-tree root-dir spec)))
  (delete-directory root-dir #:recurse))

(let ([root-dir (create-temporary-directory)]
      [spec '(foo ((bar ()) baz))])
  (test-assert "check-directory-tree twice in a row is a noop"
    (begin
      (create-directory-tree root-dir spec)
      (create-directory-tree root-dir spec)
      (check-directory-tree root-dir spec)))
  (delete-directory root-dir #:recurse))

(test-exit)
